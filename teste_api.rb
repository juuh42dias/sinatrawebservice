#SinatraRb <3

require "sinatra"
require "json"

  #Determinando rotas

  get "/" do 
  	"<p>Servidor Ativo</p>
  	Rotas: <br/>
  	/ <br/>
    /usuario <br/>
    /usuario/id <br/>
    /usuario/id/mensagem/id"
  end

  get "/usuario/:id" do
    usuario_id = params[:id]
  end
  
  get "/usuario/:id/mensagem/:mensagem_id" do 
    usuario_id = params[:id]
    mensagem_id = params[:mensagem_id]
    "#{usuario_id} #{mensagem_id}"
  end
  
  get "/usuario" do 
	  #Corresponderá a /usuario?id=algo&mensagem_id=algo
	  usuario_id = params[:id]
	  mensagem_id = params[:mensagem_id]
  end
  
  post "/usuario" do 
	  #Captura a string pura de um dado encapsulado com POST
	  dado = request.body.read
  end
  
  post "/usuario" do 
	  #Captura o hash de um dado JSON encapsulado com POST
	  dado_json = JSON.parse request.body.read
  end
  
  post "/" do 
	  dado = JSON.parse(request.body.read)
	  json dado
  end